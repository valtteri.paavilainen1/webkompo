#### Docker compose ohjeet

Dockerin ollessa päällä, suorita seuraavat komennot tässä kansiossa (juuressa):
docker-compose build
docker-compose up

Voit avata sovelluksen selaimen osoitteella:
http://localhost:8000/webkompor2/

Jos saat ilmoituksia kuten:
ERROR: for mongoserver  Cannot create container for service db: Conflict. The container name "/mongoserver" is already in use by container
"081f7a1cebf567a2bcfd825336db04870d35a4e74b516053c6fa49e3a26bbbb2". You have to remove (or rename) that container to be able to reuse

Poista olemassa oleva kontti komennolla:
docker rm "081f7a1cebf567a2bcfd825336db04870d35a4e74b516053c6fa49e3a26bbbb2"


### Vanhat ohjeet

Dockerin luonti ja käynnistys:

Siirry komentorivillä tähän kansioon.
Dockerin ollessa päällä, suorita komennot:

docker build -t docker/mongoserver:1.0 .
docker run -p 27017:27017 -it docker/mongoserver:1.0

Sovelluksen käynnistys

Avaa uusi komentorivi ja siirry tämän kansion juureen.

Jos et ole asentanut vielä node-moduuleja, tee se komennolla:

npm install

Käynnistä sovellus komennolla:

node server.js

Voit avata sovelluksen selaimen osoitteella:

http://localhost:8000/webkompor2/