app.controller('ChatCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.submitChatForm = function(isValid) {
        // Jos viesti ei ole tyhjä.
        if (isValid) {
            const username = $scope.currentUser.username;
            const message = $scope.ChatForm.chat_message.$viewValue;
            // Kutsutaan viestin lähetystä kantaan routes.js tiedostossa.
            $http({
                method: 'PUT',
                url: '/chat/sendMessage',
                data: {
                    username: username,
                    message: message,
                }
            });
            // Tyhjennetään viestikenttä.
            document.getElementById("chat_message").value = "";
            $scope.ChatForm.chat_message.$viewValue = "";
            console.log("Lähetettiin viesti: " + message);
            // Lisätään viestit viiveellä, jotta haku ehtii varmasti toteutua.
            setTimeout(
                function()
                {
                    $scope.fetchChat();
                }, 400);
        }
        else {
            // Jos viesti ei ole validi, mutta tätä funktiota onnistutaan kutsumaan.
            alert("Tarkista viestin sisältö.")
            }
    };

    // Chat viestien haku tietokannasta.
    $scope.fetchChat = function() {
        $scope.message_array = {
            messages: []
        };
        $http({
            method: 'GET',
            url: '/chat/getMessages'
        })
        .then(function (response) {
            // Lisätään löydetyt viestit message_array:hin
            if (response.data.messages !== null) {
                $scope.message_array.messages = response.data.messages;
            }
        }, function (reason) {
            console.log(reason);
        });
        // Lisätään viestit viiveellä, jotta haku ehtii varmasti toteutua.
        setTimeout(
            function()
            {
                $scope.add_messages_to_chat_log();
            }, 400);
    };

    // Formatoidaan viestin lähetys päivä hh:mm formaattiin:
    // https://stackoverflow.com/questions/19346405/jquery-how-to-get-hhmmss-from-date-object
    function time_format(d) {
        hours = format_two_digits(d.getHours());
        minutes = format_two_digits(d.getMinutes());
        return hours + ":" + minutes;
    }
    function format_two_digits(n) {
        return n < 10 ? '0' + n : n;
    }

    $scope.old_len = 0;
    // Viestien tulostus chat logiin.
    $scope.add_messages_to_chat_log = function() {
        // arrayLenght = kannassa olevien viestien määrä.
        var arrayLength = $scope.message_array.messages.length;
        const container = document.getElementById('chat_log');
        // Haetaan viestit käyttöliittymään vain jos uusia viestejä on tullut.
        if(arrayLength > $scope.old_len)
        {
            // Tyhjennetään chat_log edellisistä viesteistä.
            $('#chat_log').html($('#chat_log').data('old-state'));
            // Haetaan max 150 viimeisintä viestiä.
            for (let i = 0; i < arrayLength && i < 150; i++) {
                const raw_date = new Date($scope.message_array.messages[i]['time']);
                const time = time_format(raw_date);
                const username = $scope.message_array.messages[i]['username'];
                const message = $scope.message_array.messages[i]['message'];
                // Lisätään viestit chat_log diviin.
                container.innerHTML += '' +
                    '<div class="result" xmlns="http://www.w3.org/1999/html">' +
                    '<div><br>' +
                    (time) + '<strong> ' + (username) + '</strong>: ' + (message) +
                    '</div></div>' +
                    '</div>';
            }
            // Scrollataan chatti aina viimeisimpään viestiin viestien haun lopussa.
            const div_to_scroll = document.getElementById('chat_log_area');
            div_to_scroll.scrollTop = div_to_scroll.scrollHeight;
        }
        $scope.old_len = arrayLength;
    };

    // Kutsutaan chatin hakua aina 5 s välein.
    function automatically_fetch_chat_on_interval() {
        setInterval(
            function()
            {
                $scope.fetchChat();
            }, 5000);
    }

    // Sivustoa ladatessa haetaan chätti ja aloitetaan timeri.
    setTimeout(
        function()
        {
            $scope.fetchChat();
            automatically_fetch_chat_on_interval();
        }, 1000);
}]);