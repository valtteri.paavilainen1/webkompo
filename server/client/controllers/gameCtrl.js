app.controller('GameCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {

  var socket = io.connect('http://localhost:8000');
  //var socket = io.connect('http://a41d.k.time4vps.cloud:8000');

  // Haetaan ikkunan koko
  $scope.winWidth = $(window).width();
  $scope.winHeight = $(window).height();

  var vm = new Vue({
    el: '#vueapp',
    data: {
      bubbles: [],
      width: 400,
      height: 300,
      player: {
        x: 100,
        y: 100
      },
      mouseloc: {
        x: 0,
        y: 0
      },
      // Tätä käytetään gameserver.js tiedostossa pallon suunnan laskemiseen.
      winsize: {
          wx: $scope.winWidth,
          wy: $scope.winHeight
      },
      config: ""
    },
  methods: {
    moveBubble(evt) {
        vm.mouseloc.x = evt.clientX;
        vm.mouseloc.y = evt.clientY;
    },
    initGameboard() {
      vm.width = $scope.winWidth;
      vm.height = $scope.winHeight;
      console.log("initializing gameboard");
      var ctx = vm.$refs.canvas.getContext("2d");
      ctx.clearRect(0, 0, vm.width, vm.height);
      console.log(`plotting player (${vm.width/2},${vm.height/2}) radius ${vm.player.radius} color ${vm.player.color}`);
      vm.plotBubble((vm.width / 2), (vm.height / 2), "red", vm.player.name, vm.player.radius, ctx);
      vm.plotBubble((vm.width / 2), (vm.height / 2), "red", "asdf", 1, ctx);
    },

    renderBubbles() {
      //console.log(`rendering ${vm.bubbles.length} bubbles`);

      vm.width = $scope.winWidth;
      vm.height = $scope.winHeight;

      var ctx = vm.$refs.canvas.getContext("2d");

      ctx.clearRect(0, 0, vm.width, vm.height);

      // Plot center
      vm.plotBubble((vm.width / 2), (vm.height / 2), "black", "", 1, ctx);

      let newx = 0;
      let newy = 0;

      vm.bubbles.forEach(function(element) {
        // Render only objects visible to player
        //console.log("player", vm.player.x, vm.player.y);
        let diffx = Math.abs(vm.player.x - element.x);
        let diffy = Math.abs(vm.player.y - element.y);

        if(element.name === vm.player.name) {
          newx = element.x;
          newy = element.y;
          //console.log("element:", element, vm.player);
        }

        if(diffx < vm.width / 2 && diffy < vm.height / 2) {
          // Plot only bubbles visible to player
          // Calculate offset and plot bubble
          let topleft = {x: vm.player.x - vm.width / 2,
                         y: vm.player.y - vm.height / 2};
          vm.plotBubble(element.x - topleft.x, element.y - topleft.y, element.color, element.name, element.radius, ctx);
        }
      });

      // Set new player location
      if(newy && newx) {
        vm.player.x = newx;
        vm.player.y = newy;
        socket.emit('client:location', vm.mouseloc);
      }
    },

    plotBubble(x,y,color,name, radius,ctx) {
      ctx.beginPath();
      ctx.arc(x, y, radius, Math.PI*2, 0, true);
       // uncomment below if you want to use the same color in bubble and text
      ctx.fillStyle = color;
      if(name) {
          ctx.font = "30px Georgia";
          // TO DO: Tämä hakee kaikkien pelaajien pallojen nimeksi oman nimimerkin.
          // Pelaajien oikeat nimet (quest + num) tulevat gameserver kansion tiedostojen kautta, ne pitäisi asettaa siellä.
          //ctx.fillText($scope.currentUser.username, x - 40, y - (radius+10));
          ctx.fillText(name, x - 40, y - (radius+10));
      }
      ctx.fill();
      ctx.stroke();
    }
  },
  mounted() {
    socket.on('connect', function() {
      console.log("connected");
    });

    socket.on('game:config', function(msg) {
      // Update online players
      console.log("game:config", msg);
      vm.width = $scope.winWidth;
      vm.height = $scope.winHeight;
      vm.config = msg;
    });

    socket.on('game:sync:full', function(msg) {
      //console.log("game:sync:full", msg);
      if(msg) {
        vm.bubbles = msg;
      } else {
        console.log("bogus data from server", msg);
      }
    });

    socket.on('game:sync:partial', function(msg) {
      //console.log("game:sync:partial", msg);
      if(msg) {
        msg.forEach(function(element) {
          vm.bubbles[element.id] = element;
        });
      } else {
        console.log("bogus data from server", msg);
      }
      vm.renderBubbles();
    });

    socket.on('game:init', function(player) {
      // Plot bubbles on canvas
      console.log("got init", player);
      vm.player = player;
      vm.initGameboard();
    });

    // Kun gameserver.js lähettää game:eat signaalin, lisätään pelaajan scorea.
    socket.on('game:eat', function() {
        // Kutsutaan scoren päivitystä scoreCtrl:llerista.
        // TO DO: Käytetään myös tässä online players scorea.
        vm.player.score ++;
        $rootScope.$emit("trigger_add_score");
        //console.log(vm.player.name + " " + vm.player.score)
    });

      // Lähettää playersCtrl.js tiedostoon player listan.
    socket.on('game:getPlayers', function(players) {
        $rootScope.$emit("trigger_list_players", players);
    });

    // Pelaajan nimen lähetys gameserver.js tiedostoon.
    $scope.changeName = function() {
        vm.player.name = $scope.currentUser.username;
        socket.emit('client:setName', vm.player.name);
    };

    // Päivitetään nimi aina kun se muutetaan userCtrl.js tiedostossa.
    $rootScope.$on("trigger_set_name", function () {
        $scope.changeName();
    });

    // Kun ikkunan koko muuttuu. Timer estää funktion kutsun koon muuttamisen aikana.
      let resizeTimer;
      $(window).on('resize', function(e) {
          clearTimeout(resizeTimer);
          resizeTimer = setTimeout(function(msg) {
              // Asetetaan ikkunan mitat ja lähetetään ne gameserver.js tiedostoon.
              $scope.winWidth = $(window).width();
              $scope.winHeight = $(window).height();
              vm.winsize.wx = $scope.winWidth;
              vm.winsize.wy = $scope.winHeight;
              socket.emit('client:winsize', vm.winsize);
              // Uudelleenpiirretään pelaajan kupla (+ muut) ruudun keskelle.
              vm.renderBubbles();
          }, 250);
      });
      // Lähetetään ikkunan mitat ja nimimerkki myös sivua ladatessa.
      setTimeout(
          function()
          {
              socket.emit('client:winsize', vm.winsize);
              $scope.changeName();
          }, 200);
    }
  });
}]);