app.controller('PlayersCtrl', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {


    $rootScope.$on('trigger_list_players',function (event, players) {
        // Järjestetään parametrina saatu players taulukko score sarakkeen mukaan.
        players.sort(function (a, b) {
            return b.score - a.score;
        });
        const arrayLength = players.length;
        const container = document.getElementById('online_players');
        // Tyhjennetään edelliset arvot.
        $('#online_players').html($('#online_players').data('old-state'));
        // Tulostetaan lista online pelaajista.
        let score_ranking = 1;
        for (let i = 0; i < arrayLength; i++) {
            const username = players[i]['name'];
            const score = players[i]['score'];
            // Lisätään scoret leaderboard diviin.
            container.innerHTML += '' +
                '<div class="result" xmlns="http://www.w3.org/1999/html">' +
                '<div>' +
                '<strong>' + (score_ranking) + '. ' + (username) + ': ' + (score) + '</strong>' +
                '</div></div>' +
                '</div>';
            score_ranking++;
        }
    });
}]);