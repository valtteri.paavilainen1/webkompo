app.controller('ScoreCtrl', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {

    // Alustetaan tarvittavat muuttujat
    $scope.score = 0;
    $scope.end_game_triggered = false;

    // Kun pelaaja popsii palloja, kutsutaan tämä funktio agarioCtrl.js tiedostosta.
    $rootScope.$on("trigger_add_score", function () {
        $scope.score = $scope.score + 1;
        // Jos pelaajan pallo kasvaa liian suureksi, lopetetaan peli. Koska useita palloja voidaan
        // syödä samanaikaisesti tarvitaan boolean tarkistamaan ettei peliä ole jo lopetettu.
        if($scope.score > 8999 && $scope.end_game_triggered === false)
        {
            $scope.end_game();
        }
    });

    $scope.end_game = function() {
        // Tarkistetaan vielä uudestaan, ettei peliä ole jo kutsuttu lopetettavaksi.
        // Jos tarkistusta ei tehdä, saatetaan koodi joskus suorittaa kahdesti.
        if($scope.end_game_triggered === false)
        {
            const username = $scope.currentUser.username;
            let score = $scope.score;
            if(score > 8999)
            {
                score = score + Math.floor(Math.random() * 100) + 1;
                alert("It's over 9000!!!! Pallosi kasvoi liian suureksi ja poksautimme sen. Scoresi on: 9000 + randomilla " +
                    "generoitu numero 0 ja 100 välillä, näin vältämme duplikaatit scoret. Lopullinen scoresi: " + score)
            }
            else
            {
                alert("Peli päättyi, scoresi: " + score)
            }
            // Lisätään score tietokantaan.
            $http({
                method: 'PUT',
                url: '/score/addScore',
                data: {
                    username: username,
                    score: score,
                }
            });
            location.reload(true);
        }
        $scope.end_game_triggered = true;
    };

    // High scorejen haku kannasta.
    $scope.fetchScores = function() {
        $scope.score_array = {
            scores: []
        };
        $http({
            method: 'GET',
            url: '/score/getScores'
        })
            .then(function (response) {
                // Lisätään löydetyt viestit score_array:hin
                if (response.data.scores !== null) {
                    $scope.score_array.scores = response.data.scores;
                }
            }, function (reason) {
                console.log(reason);
            });
        // Lisätään viestit viiveellä, jotta haku ehtii varmasti toteutua.
        setTimeout(
            function()
            {
                $scope.add_scores_to_leaderboard();
            }, 300);
    };

    // Leaderboardin päivitys
    $scope.add_scores_to_leaderboard = function() {
        var arrayLength = $scope.score_array.scores.length;
        const container = document.getElementById('leaderboard');
        // Tyhjennetään leaderboardin edelliset arvot.
        $('#leaderboard').html($('#leaderboard').data('old-state'));
        // Lisätään scoret leaderboardiin.
        let score_ranking = 1;
        for (let i = 0; i < arrayLength; i++) {
            const username = $scope.score_array.scores[i]['username'];
            const score = $scope.score_array.scores[i]['score'];
            // Lisätään scoret leaderboard diviin.
            container.innerHTML += '' +
                '<div class="result" xmlns="http://www.w3.org/1999/html">' +
                '<div>' +
                '<strong>' + (score_ranking) + '. ' + (username) + ': ' + (score) + '</strong>' +
                '</div></div>' +
                '</div>';
            score_ranking++;
        }
    };

    // Kutsutaan highscoren päivitys aina 15 s välein.
    function automatically_fetch_score_on_interval() {
        setInterval(
            function()
            {
                $scope.fetchScores();
            }, 15000);
    }

    // Sivustoa ladatessa haetaan scoret ja aloitetaan timeri.
    setTimeout(
        function()
        {
            $scope.fetchScores();
            automatically_fetch_score_on_interval();
        }, 300);
}]);