'use strict';
// Määritellään app, myös muut kontrollerit hyödyntävät tätä.
var app = angular.module('app', []);

// Tätä Controlleria kutsutaan yleensä nimellä "app", mutta nimetään se nyt UserCtrl:lleriksi.
app.controller('UserCtrl', ['$scope', '$rootScope', '$http', '$window', '$location',
    function ($scope, $rootScope, $http, $window, $location) {

    $scope.logout = function() {
        $http({
            method: 'GET',
            url: '/webkompor2/logout'
        })
            .then(function() {
                $window.location.href = '/webkompor2/'
            })
    };

    $scope.currentUser = {};

    $scope.getCurrentUser = function() {
        $http({
            method: 'GET',
            url: '/webkompor2/getCurrentUserData'
        })
            .then(function(response) {
                $scope.currentUser = response.data.user;
                console.log("Sisäänkirjautuminen käyttäjänä: " + $scope.currentUser.username + " | " + $scope.currentUser.email)
            }, function(err) {
                console.log(err)
            });
    };
    $scope.getCurrentUser();


    $scope.load_user_data = function() {
        // Haetaan app.js:ssä haetut käytttäjän tiedot popupin arvoiksi.
        document.getElementById("user_email").value = $scope.currentUser.email;
        document.getElementById("user_username").value = $scope.currentUser.username;
        // TO DO: Salasanakenttien resetointi... tässä jotain häikkää:
        //$scope.PasswordForm.current_pass.$pristine = true;
        //$scope.PasswordForm.new_pass.$pristine = true;
        //$scope.PasswordForm.confirm_new_pass.$pristine = true;
    };

    $scope.submitNicknameForm = function(isValid) {
        if (isValid) {
            const username = $scope.NicknameForm.user_username.$viewValue;
            $http({
                method: 'POST',
                url: '/webkompor2/changeNickname',
                data: {
                    _id: $scope.currentUser._id,
                    username: username,
                }
            })
            .then(function(response) {
                if(response.data.error)
                {
                    alert("Tapahtui virhe: " + response.data.message);
                }
                else
                {
                    $scope.currentUser.username = username;
                    $rootScope.$emit("trigger_set_name");
                    console.log("Muutetiin käyttäjänimeksi: " + $scope.currentUser.username);
                    alert("Nimimerkki vaihdettiin onnistuneesti.");
                }

            }, function(error) {
                console.log(error);
            });
        }
        else {
            const username = $scope.NicknameForm.user_username.$pristine = false;
            }
    };

    $scope.submitPasswordForm = function(isValid) {
        // Lähetetään formi jos se on validi.
        if (isValid) {
            // Haetaan varaukseen tulevien tietojen arvot.
            const password = $scope.PasswordForm.current_pass.$viewValue;
            const new_password = $scope.PasswordForm.new_pass.$viewValue;
            const password_confirm = $scope.PasswordForm.confirm_new_pass.$viewValue;
            const numRegex = ".*[0-9].*";
            const alphaRegex = ".*[A-Z].*";
            if (password !== "" && new_password.match(numRegex) && new_password.match(alphaRegex) &&
                password_confirm.length > 5) {
                $http({
                    method: 'POST',
                    url: '/webkompor2/changePassword',
                    data: {
                        currentPassword: password,
                        newPassword: new_password,
                        newPasswordConfirm: password_confirm
                    }
                })
                    .then(function (response) {
                        if (response.data.error)
                            alert("Tapahtui virhe: " + response.data.message);
                        else
                            alert("Salasana vaihdettiin onnistuneesti.");
                    }, function (error) {
                        console.log(error);
                    });
            }
            else {
                alert("Salasanan on oltava vähintään 6 merkkiä pitkä ja sen täytyy sisältää ainakin 1 numero ja iso kirjain.")
            }
        }
    };
}]);