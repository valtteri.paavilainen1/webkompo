/******** MIT-lisenssi: ks. License.txt ********/

/* Moduulit */

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

/* Skeemat */
var User = require('./schemas/user.js');
var HighScore = require('./schemas/high_score.js');
var Chat = require('./schemas/chat.js');

module.exports = {
    init: function() {
        /* Yhdistetään Dockerissa pyörivään tietokantaan. Se luodaan, ellei sitä ole. */
        mongoose.connect('mongodb://mongoserver/data/db:27017');
    }
};