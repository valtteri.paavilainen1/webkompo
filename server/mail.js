'use strict';
var nodemailer = require('nodemailer');
var smtp = require('./smtpaddress.js');

var transporter = nodemailer.createTransport(smtp);

module.exports = {
  
  /* send() lähettää sähköpostin.
  Parametriolion täytyy sisältää nämä tiedot: to, subject, text. */
  
  send: (mailOptions) => {
    
    if (!mailOptions.to) {
      return console.log('Viestin vastaanottaja puuttuu.');
    }
    if (!mailOptions.subject) {
      return console.log('Viestin otsikko puuttuu.');
    }
    if (!mailOptions.text) {
      return console.log('Viestin plain text -muotoinen sisältö puuttuu.');
    }
    /*
    if (!mailOptions.html) {
      return console.log('Viestin HTML-muotoinen sisältö puuttuu.');
    }
    */

    transporter.sendMail({
      from: '"Webkompor2" <kareliatestimaileri@gmail.com>',
      to: mailOptions.to,
      subject: mailOptions.subject,
      attachments: mailOptions.attachments,
      text:
        mailOptions.text + '\n\n' +
        'Tähän viestiin ei voi vastata.'
      //, html: mailOptions.html
    }, (error, info) => {
      console.log(error || info);
    });
  }
};
