'use strict';

const LocalStrategy = require('passport-local').Strategy;

const User = require('./schemas/user.js');
const randomstring = require('randomstring');

module.exports = (passport) => {
	passport.serializeUser((user, done) => {
		done(null, user.id);
	});
	
	passport.deserializeUser((id, done) => {
		User.findById(id, (err, user) => {
			done(err, user);
		});
	});
	
	passport.use('local-login', new LocalStrategy({
		
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
		
	}, (req, email, password, done) => {
		User.findOne({'email': email}, (err, user) => {
			if(err)
				return done(err);
			
			if(!user)
				return done(null, false, req.flash('loginMessage', 'Käyttäjätiliä ei löytynyt!'));
			
			if(!user.validPassword(password))
				return done(null, false, req.flash('loginMessage', 'Salasana oli virheellinen!'));
			
			if(!user.verified)
				return done(null, false, req.flash('loginMessage', "Tätä käyttäjää ei ole vielä vahvistettu!"));
			
			return done(null, user);
		});
	}));
}