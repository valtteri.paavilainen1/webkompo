'use strict';

const express = require('express');
const router = express.Router();

const Chat = require('../schemas/chat.js');

router.put('/sendMessage', (req, res) => {
    // Chat viestin talletus tietokantaan. (jos viesti/username eivät tyhjiä)
    try {
        if(req.body.username && req.body.message)
        {
            let item = new Chat({
                username: req.body.username,
                message: req.body.message,
                time: new Date(),
            });
            res.end();
            return item.save();
        }
    }
    catch(e) {
        console.log("Viestiä lähettäessä tapahtui virhe: " + e);
        res.end();
    }
});

// Kaikkien viestien haku tietokannasta.
router.get('/getMessages', function(req, res) {

    // viesti array ja promise paluuarvoksi.
    let messages = [];
    let promises = [];
    Chat.find()
        .then((messagesFound) => {
            messages = messagesFound;
            for(let i=0;i<messages.length;i++) {
                promises.push(messages[i]);
            }
            return Promise.all(promises);
        })
        .then((messages) => {
            res.send({
                messages: messages,
            });
        })
        .catch((reason) => {
            res.send({
                messages: null,
                error: reason
            });
        });
});

module.exports = router;