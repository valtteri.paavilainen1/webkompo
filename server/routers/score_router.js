'use strict';

const express = require('express');
const router = express.Router();

const Score = require('../schemas/high_score.js');

router.put('/addScore', (req, res) => {
    // Scoren talletus tietokantaan. (jos username/score eivät tyhjiä)
    try {
        if(req.body.username && req.body.score)
        {
            let item = new Score({
                username: req.body.username,
                score: req.body.score,
            });
            res.end();
            return item.save();
        }
    }
    catch(e) {
        console.log("Scorea tallentaessa tapahtui virhe: " + e);
        res.end();
    }
});

// Scorejen haku tietokannasta.
router.get('/getScores', function(req, res) {

    // Array ja promise paluuarvoksi.
    let scores = [];
    let promises = [];
    // Haetaan 10 parasta tulosta ja järjestetään ne scoren mukaan.
    Score.find().limit( 10 ).sort( { score: -1 })
        .then((scoresFound) => {
            scores = scoresFound;
            for(let i=0;i<scores.length;i++) {
                promises.push(scores[i]);
            }
            return Promise.all(promises);
        })
        .then((scores) => {
            res.send({
                scores: scores,
            });
        })
        .catch((reason) => {
            res.send({
                scores: null,
                error: reason
            });
        });
});

module.exports = router;