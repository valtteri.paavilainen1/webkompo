'use strict';

const express = require('express');
const router = express.Router();
const path = require('path');
const passport = require('passport');
const session = require('express-session');
const flash = require('connect-flash');
const randomstring = require('randomstring');

const mail = require('../mail');
const domain = require('../domain');
require('../passport')(passport);

const User = require('../schemas/user.js');

router.use(session({secret: 'top-secret'}));
router.use(passport.initialize());
router.use(passport.session());
router.use(flash());

router.get('/', isLoggedIn, (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'game.html'));
});

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/webkompor2/login');
});

router.get('/login', (req, res) => {
    res.render('login', { message: req.flash('loginMessage'), message2: req.flash('successMessage') });
});

router.post('/login', passport.authenticate('local-login', {
    successRedirect: '/webkompor2/',
    failureRedirect: '/webkompor2/login',
    failureFlash: true
}));

// Rekisteröinti
router.post('/register', (req, res) => {
    const verificationToken = randomstring.generate(64);
    const numRegex   = ".*[0-9].*";
    const alphaRegex = ".*[A-Z].*";
    const to_verify = req.body.password.toString();
    User.find({'email': req.body.email})
        .then((result) => {
        if(result.length !== 0)
            throw new Error('Tällä sähköpostilla on jo luotu tunnus.');
    if(!to_verify.match(numRegex) || !to_verify.match(alphaRegex) || to_verify.length < 6)
        throw new Error('Salasanan on oltava vähintään 6 merkkiä pitkä ja sen täytyy sisältää ainakin 1 numero ja iso kirjain.');
    if(req.body.password !== req.body.passwordConfirm)
        throw new Error('Salasanat eivät täsmää.');
    if(req.body.username === "")
        throw new Error("Nimimerkki on pakollinen kenttä.");
    if(req.body.email === "")
        throw new Error("Sähköposti on pakollinen kenttä.");
    else {
        let user = new User({
            username: req.body.username,
            email: req.body.email,
            verified: false,
            verificationToken: verificationToken
        });
        user.password = user.generateHash(req.body.password);
        return user.save();
    }
    })
    .then((user) => {
        if(!user)
            throw new Error("Tietokantavirhe: tarkista yhteys tietokantaan.");
    else {
        // Sähköpostin lähetys käyttäjän osoitteeseen.
        let msg = {
            to: user.email,
            subject: 'Vahvista rekisteröinti',
            text: 'Vahvista rekisteröitymisesi käyttäjälle: ' + user.email +
            '\nVahvistus tapahtuu klikkaamalla alla näkyvää linkkiä.\n' +
            domain + '/webkompor2/verify/' + verificationToken
        };
        mail.send(msg);
        res.render('login', {message: "", message2: `Rekisteröinti onnistui. 
                Vahvistuslinkki lähetetty osoitteeseen: ${user.email}`});
    }
    })
    .catch((reason) => {
        res.render('login', {message: "Rekisteröinti epäonnistui, virhe: " + reason.message, message2: ''});
        });
});

// Tilin vahvistussivun toiminnallisuus.
router.get('/verify/:token', (req, res) => {
    User.findOne({'verificationToken': req.params.token})
    .then((user) => {
        if(!user)
            res.render('verify', { messageFailed: 'Käyttäjätiliä ei löydetty.', messageSuccess: ''});
        if(user.verified === true)
            res.render('verify', { messageFailed: 'Käyttäjätili on jo vahvistettu.', messageSuccess: ''});
        else {
            user.verified = true;
            user.verificationToken = null;
            return user.save()
        }
    })
    .then(() => {
        res.render('verify', { messageFailed: '', messageSuccess: 'Käyttäjätili vahvistettiin onnistuneesti!'});
    })
    .catch((reason) => {
        console.log(reason);
    });
});

// RecoveryPage
router.get('/recoveryPage', (req, res) => {
    res.render('recovery');
});

// Salasanan palautus sähköpostiin.
router.post('/recoverPassword', (req, res) => {
    const newPass = randomstring.generate(10);
    User.findOne({email: req.body.email})
        .then((user) => {
            if(!user)
                throw new Error("Käyttäjää ei löytynyt.");
            console.log(user);
            user.password = user.generateHash(newPass);
            return user.save();
        })
        .then((user) => {
            let msg = {
                to: user.email,
                subject: 'Uusi salasana',
                text: 'Tilillesi on luotu uusi salasana:\n\n' + newPass +
                '\n\nKirjaudu sisään käyttäen yläpuolella olevaa salasanaa ja vaihda salasanasi heti sisäänkirjautumisen jälkeen.\n'
            };
            mail.send(msg);
            res.render('login', {message: "", message2: "Uusi salasana lähetetty sähköpostiin. " +
            "Muista vaihtaa salasana välittömästi kirjauduttuasi."})
        })
        .catch((reason) => {
            res.render('login', {message: "Salasanan palautus epäonnistui: " + reason.message, message2: ""});
        });
});

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated())
        return next();
    res.redirect('/webkompor2/login');
}

router.get('/getCurrentUserData', isLoggedIn, (req, res) => {
    res.send({
        user: {
            username: req.user.username,
            email: req.user.email
        }
    });
});

function hideValuableData(user) {
    return new Promise((resolve, reject) => {
        const newUser = {
            username: user.username,
            email: user.email
        };

        if (typeof newUser.username === 'string' && typeof newUser.email === 'string') {
            return resolve(newUser);
        } else {
            return reject({
                message: "Käyttäjän tietojen haku epäonnistui."
            });
        }
    })
}

router.post('/changeNickname', isLoggedIn, (req, res) => {
    User.findById(req.user._id)
        .then((user) => {
            user.username = req.body.username;
            res.send({
                error: false,
                message: "Nimimerkki muutettiin onnistuneesti."
            });
            return user.save();
        })
        .catch((reason) => {
            res.send({
                error: true,
                message: reason.message
            });
        });
});

router.post('/changePassword', isLoggedIn, (req, res) => {
    User.findById(req.user._id)
    .then((user) => {
    if(!user.validPassword(req.body.currentPassword))
    {
        throw new Error('Nykyinen salasanasi on väärin..');
    }
    if(req.body.newPassword !== req.body.newPasswordConfirm)
            throw new Error('Varmista, että uusi salasana on sama myös vahvistuskentässä.');
        user.password = user.generateHash(req.body.newPassword);
        return user.save();
    })
        .then((user) => {
            if(!user)
                throw new Error('Salasanan vaihto epäonnistui.');
            res.send({
                error: false,
                message: "Salasanan vaihto onnistui!"
            });
    })
    .catch((reason) => {
        res.send({
        error: true,
        message: reason.message
        });
    });
});

module.exports = router;