'use strict';

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var ChatSchema = new Schema({
	username: { type: String, required: true, index: { unique: false } },
    message: { type: String, required: true, },
    time: { type: Date, required: true, },
});

module.exports = mongoose.model('Chat', ChatSchema);