'use strict';

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var HighScoreSchema = new Schema({
	username: { type: String, required: true, index: { unique: false } },
	score: { type: Number, required: true },
});

module.exports = mongoose.model('HighScore', HighScoreSchema);