'use strict';

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

var UserSchema = new Schema({
    email: { type: String, required: true, index: { unique: true } },
    username: { type: String, required: true },
    password: { type: String, required: true },
	verificationToken: String,
    verified: { type: Boolean, default: false },
});

UserSchema.methods.generateHash = (password) => {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function(password) {
  	return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);