"use strict";
const express = require('express');
const bodyParser = require('body-parser');
const db = require('./db');
const app = express();
const router = express.Router();
const user_router = require('./routers/user_router');
const score_router = require('./routers/score_router');
const chat_router = require('./routers/chat_router');
db.init();

const HOST = "localhost";
const PORT = 8000;

// Add websocket support
const server = require('http').createServer(app);
var wss = require('socket.io')(server);

// Start game server
const GameServer = new (require("./gameserver/gameserver"))(wss);

app.use('/webkompor2', express.static(__dirname + '/client'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use('/webkompor2/', user_router);
app.use('/score/', score_router);
app.use('/chat/', chat_router);
app.set('view engine', 'ejs');

server.listen(PORT, function () {
    console.log('server up @ http://' + HOST + ":" + PORT);
});