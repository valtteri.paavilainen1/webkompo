"use strict";

//Tarvitaan chai, mocha ja supertest
//npm install -g mocha
//npm install -g chai
//npm install supertest --save-dev

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;

const request = require('supertest');
const chai = require('chai');
const expect = chai.expect;
let chaiHttp = require('chai-http');

const express = require('express');
const router = express.Router();
const Chat = require('../schemas/chat.js');
let server = require('../server.js');

let kokeilija = {
    username: "KokeilijaKäyttäjä",
    message: "moi",
    time: new Date()
};
let message = kokeilija.message;
describe('Chat related tests', function() {
    describe('Chat PUT', () => {
        it('should send message and save it', function (done) {
            request('http://localhost:8000')
                // Put polku
                .post('/webkompor2/sendmessage')
                .send(message)
                .expect((res) => {
                expect(res.body).to.include(message);
            console.log(message)
        })
        .end(function (err, res) {
                if (err)
                    console.log(kokeilija.time + " " + kokeilija.username + ": " + kokeilija.message);
                return done();
            });
        });
});
});

// Testailu getin kanssa kesken
// describe('Chat GET', ()=> {
//     it('should fetch messages', function (done) {
//         request('http://localhost:8000')
//             .get('/webkompor2/getMessages') // Määritellään HTTP GET -kutsun polku
//            .end(function (err, res) {
//                 expect(err).to.be.null;
//                 expect(res.body).to.include(message);
//                 done();
//             });
//     });
// });

