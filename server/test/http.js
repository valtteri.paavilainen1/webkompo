var chai = require('chai');
var expect = require('chai').expect;
var chaiHttp = require('chai-http');

var server = require('../server.js');

chai.use(chaiHttp);

describe('HTTP server related tests', function () {
    describe('HTTP GET', function () {
        it('Should return status 200', function (done) {
            chai.request('http://localhost:8000')
                // Määrittele HTTP GET -kutsun polku
                .get('/')
                // Määrittele HTTP GET -kutsun polku
                .get('/webkompor2/login')
                .end(function (err, res) {
                    expect(err).to.be.undefined;
                    expect(res).to.have.status(200);
                    done();
                });
        });
        it('Should return status other than 200', function (done) {
            chai.request('http://localhost:8000')
                // Määrittele HTTP GET -kutsun polku
                .get('/polkujotaeiole')
                .end(function (err, res) {
                    expect(err).not.to.be.null;
                    expect(res).to.have.status(404);
                    done();
                });
        });
    });
    describe('HTTP POST', function () {

    });
});
