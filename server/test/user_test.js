"use strict";

// Tarvitaan chai, mocha ja supertest
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const chai = require('chai');
const expect = chai.expect;


// Luodaan uusi schema joka hyväksyy nimi objektin
// 'usernamename', 'email', 'password' on vaadittu kenttä
const testSchema = new Schema({
    username: {type: String, required: true},
    email:    {type: String, required: true, unique: true},
    password: {type: String, required: true}
});

const tiedot = mongoose.model('tiedot', testSchema);
describe('Database tests', function() {
    it('should open connection to database', function (done) {
        // Ennen testauksen aloitusta, luodaan 'hiekkalaatikko' tietokanta yhteys
        // Kun yhteys on muodostettu, lähdetään suorittamaan

        mongoose.connect('mongodb://localhost/testDatabase');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {

            done();
        });
        console.log('Yhdistetty tietokantaan!');
    });
});

describe('Test database', function() {
    // Tallennetaan objektit 'usernamename', 'email', 'password' ja asetetaan niille arvot
    // ota huomioon pakollisuudet require: true kaikissa.

    it('should add new username,email and password to the database for testing', function (done) {
        let testInfo = tiedot({
            username: 'TestiBotti',
            email: 'testi@botti.fi',
            password: 'testi1234'
        });
        testInfo.save();
        console.log('tiedot lisätty');
        done();
    });

});

it('should NOT save incorrect format to database', function(done){
    // Yritetään tallentaa kelpaamatonta infoa, pitäisi heittää error
    let wrongSave = tiedot({
        notName: 'Not TestiBotti',
        notEmail: 'tamaeiole.fi.email',
        notPassword: 'tömmöinen. ei voi. olla'
    });

    wrongSave.save(err => {
        if(err){return done(); }
        throw new Error('Should generate error!');
});
});

it('should retrieve data from test database', function(done) {
    // Yrittää löytää käyttäjän joka aiemmin luotiin
    tiedot.find({username: 'TestiBotti'}, (err, username) => {
        if (err) {
            throw err;
        }
        if (username.length === 0) {
        throw new error('No Data!');
    }

});
    done();
});

// Kaikkien testien jälkeen suljetaan yhteys
after(function(done){
    mongoose.connection.close(done);
});



